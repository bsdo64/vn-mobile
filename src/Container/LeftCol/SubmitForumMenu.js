import React from 'react';
import SubmitForumMenu from '../../Components/LeftMenus/SubmitForumMenu';

const SubmitCategoryMenu = React.createClass({
  displayName: 'SubmitCategoryMenu',
  render() {
    return (<SubmitForumMenu />)
  }
});

module.exports = SubmitCategoryMenu;